FROM node

MAINTAINER Rafael Martins rsmartins@indracompany.com

RUN printf "deb http://httpredir.debian.org/debian jessie-backports main non-free\ndeb-src http://httpredir.debian.org/debian jessie-backports main non-free" > /etc/apt/sources.list.d/backports.list && \
	apt-get update -qqy && \
	apt-get install -y --no-install-recommends \
	ffmpeg \
	imagemagick \
	git \
	mongodb \
	supervisor && \
	apt-get clean && \
    rm -rf /var/lib/apt/lists/* 
	
RUN useradd -ms /bin/bash pisignage && \
	git clone https://github.com/colloqi/pisignage-server && \
	mkdir -pv media/_thumbnails && \
	cd pisignage-server && \
	chown pisignage:pisignage data/ && \
	npm install
	
ADD content/ /
	
EXPOSE 3000

VOLUME  ["/var/lib/mongodb","/var/log/mongodb"]

ENTRYPOINT ["/opt/run"]
